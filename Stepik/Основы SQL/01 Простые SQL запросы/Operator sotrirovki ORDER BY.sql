CREATE TABLE products 
(
    id INT NOT NULL PRIMARY KEY,
    name NVARCHAR(255) NULL,
    count INTEGER NULL,
    price INTEGER NULL
);
INSERT INTO products (id, name, count, price)
VALUES
    (1, 'Стиральная машина', 5, 10000),
    (2, 'Холодильник', 0, 10000),
    (3, 'Микроволновка', 3, 4000),
    (4, 'Пылесос', 2, 4500),
    (5, 'Вентилятор', 0, 700),
    (6, 'Телевизор', 7, 31740),
    (7, 'Тостер', 2, 2500),
    (8, 'Принтер', 4, 3000),
    (9, 'Активные колонки', 1, 2900);

CREATE TABLE users 
(
    id INT NOT NULL PRIMARY KEY,
    first_name NVARCHAR(50) NULL,
    last_name NVARCHAR(50) NULL,
    birthday DATE NULL,
    salary INTEGER NULL,
    job NVARCHAR(50) NULL
);
INSERT INTO users (id, first_name, last_name, birthday, salary, job)
VALUES
    (1, 'Дмитрий', 'Петров', '2000-03-14', 25000, 'офис-менеджер'),
    (2, 'Ольга', 'Антонова', '1999-12-01', 41000, 'дизайнер'),
    (3, 'Сергей', 'Васильев', '2002-02-20', 40000, 'младший программист'),
    (4, 'Константин', 'Степаниденко', '2004-03-07', 30000, 'водитель'),
    (5, 'Алена', 'Шикова', '1999-08-17', 53000, 'фотограф'),
    (6, 'Василина', 'Парамонова', '2000-10-10', 28000, 'секретарь'),
    (7, 'Александр', 'Пузаков', '2002-02-20', 120000, 'ведущий программист'),
    (8, 'Алина', 'Антонова', '2002-01-01', 40000, 'верстальщик');
	

--если сортировать по двум и более полям, то сперва сортировка по первому, а далее по второму полю относительно первого
--DESC - сортирует в обратном порядке

--Выберите из таблицы products все товары в порядке возрастания цены (price).
select * from products order by products.price

--Выберите сотрудников из таблицы users с зарплатой (salary) меньше 30000 и отсортируйте данные по дате рождения (birthday). Сотрудников с нулёвой зарплатой выбирать не нужно.
select * from users where users.salary <= 30000 and users.salary <> 0 order by users.birthday

--Выберите из таблицы users всех пользователей с зарплатной от 40000 рублей и выше. Данные нужно сначала отсортировать по убыванию зарплаты (salary), а затем в алфавитном порядке по имени  (first_name)
select * from users where users.salary >= 40000 order by users.salary desc, users.first_name

--Выберите из таблицы products все товары с порядке убывания цены. Выведите только имена (name) и цены (price).
select products.name, products.price from products order by products.price desc

--Выберите из таблицы products все товары стоимостью 5000 и выше в порядке убывания цены (price).
select * from products where products.price >= 5000 order by products.price desc

--Выберите из таблицы products все товары стоимостью до 3000 рублей отсортированные в алфавитном порядке. Вывести нужно только имя (name), количество (count) и цену (price)
select products.name, products.count, products.price from products where products.price <= 3000 order by products.name

--Выберите из таблицы users фамилии (last_name) и имена (first_name) всех пользователей. Данные должны быть отсортированы сначала по фамилии, а затем по имени
select users.last_name, users.first_name from users order by users.last_name, users.first_name

