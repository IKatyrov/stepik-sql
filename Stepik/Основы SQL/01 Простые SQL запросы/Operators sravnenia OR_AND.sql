CREATE TABLE orders (
    id INT NOT NULL PRIMARY KEY,
    user_id INTEGER NULL,
    products_count INTEGER NULL,
    sum INTEGER NULL,
    status VARCHAR(20) NULL
);
INSERT INTO orders (id, user_id, products_count, sum, status)
VALUES
    (1, 1, 2, 1300, 'new'),
    (2, 18, 1, 200, 'cancelled'),
    (3, 11, 1, 2140, 'in_progress'),
    (4, 145, 5, 6800, 'new'),
    (5, 23, 1, 999, 'new'),
    (6, 1, 2, 7690, 'cancelled'),
    (7, 17, 1, 1600, 'new'),
    (8, 5, 4, 400, 'delivery'),
    (9, 2355, 1, 1450, 'new'),
    (10, 13, 7, 13000, 'returned'),
    (11, 7, 3, 3000, 'returned'),
    (12, 8, 1, 3000, 'new');
	

--operator AND eto uslovie "odnovremennosti", takghe u operatora AND stoit povishenniy prioritet pered OR

--in oznachaet gde orders.status raven kakomu-libo znacheniu v skobkah

--viberite iz tablicy orders vse zakazi krome otmenonnyh. u otmenonnyh zakazov status raven "cancelled"
select * from  orders where orders.status in ('new', 'in_progress','delivery','returned') 

--viberite iz tablicy orders vse zakazi soderzhashie bolee 3 tovarod (products_count). vivesti nuzhno tolko nomer (id) i summu (sum) zakaza
select orders.id, orders.sum from orders where orders.products_count > 3

--Vyberite iz tablicy orders vse otmenennye zakazy. U otmenennyh zakazov status raven "cancelled".
select * from orders where orders.status = 'cancelled'

--Vyberite iz tablicy orders vse zakazy, u kotoryh summa (sum) bolshe 3000 ili kolichestvo tovarov (products_count) ot 3 i bolshe.
select * from orders where orders.sum >= 3000 or orders.products_count >= 3 

--Vyberite iz tablicy orders vse zakazy, u kotoryh summa (sum) ot 3000 i vyshe, a kolichestvo tovarov (products_count) menshe 3.
select * from orders where orders.sum >= 3000 and orders.products_count < 3 

--Vyberite iz tablicy orders vse otmenennye (cancelled) i vozvrashchennye (returned) tovary. Ispolzujte IN.
select * from orders where orders.status in ('cancelled','returned') 

-- Vyberite iz tablicy orders vse otmenennye zakazy isklyuchaya zakazy stoimostyu ot 3000 do 10000 rublej vkluchitelno
select * from orders where orders.sum >= 3000 and orders.sum <= 10000 

--Vyberite iz tablicy orders vse otmenennye zakazy stoimost'yu ot 3000 do 10000 rublej vkluchitelno. Ispolzujte BETWEEN
select * from orders where orders.status = 'cancelled' and (orders.sum between 3000 and 10000) 

