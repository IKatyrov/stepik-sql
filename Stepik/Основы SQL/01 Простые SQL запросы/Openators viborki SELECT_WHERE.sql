CREATE TABLE products 
(
    id INTEGER  NOT NULL PRIMARY KEY,
    name VARCHAR(255) NULL,
    count INTEGER NULL,
    price INTEGER NULL
);
INSERT INTO products (id, name, count, price)
VALUES
    (1, '���������� ������', 5, 10000),
    (2, '�����������', 0, 10000),
    (3, '�������������', 3, 4000),
    (4, '�������', 2, 4500),
    (5, '����������', 0, 700),
    (6, '���������', 7, 31740),
    (7, '������', 2, 2500),
    (8, '�������', 4, 3000);

--vzya' vse dannye iz tablicy products
select * from products 

--vzyat' imya i cenu iz products
select products.name, products.price from products 

--vzyat imya, kol-vo i cenu iz products
select products.name, products.count, products.price from products 

--vzyat imya, kol-vo i cenu iz products gde kol-vol bolshe libo raven 3
select products.name, products.count, products.price from products where products.count >= 3 

--vzyat vse zapisi cena kotoryh menshe 3000
select * from products where products.price < 3000 

--vzyat iz tablicy products imena (name) i ceny (price) vsekh tovarov, stoimostyu ot 10 000 i vyshe.
select products.name, products.price from products where products.price > 10000 

--poluchit iz tablicy products imena (name) tovarov, kotorye zakonchilis
select products.name from products where products.count = 0 

--vybrat iz tablicy products nazvanie (name) i ceny (price) tovarov, stoimostyu do 4000 vklyuchitelno
select products.name, products.price from products where products.price <= 4000 
