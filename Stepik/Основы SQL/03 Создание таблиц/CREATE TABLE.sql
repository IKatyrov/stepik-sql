DROP TABLE IF EXISTS rating, orders, products, users, messages;

create table rating
(
	id int primary key identity (1,1) not null,
	car_id int not null,
	user_id int not null,
	rating float not null
);

insert into 
	rating (car_id,user_id,rating)
values 
	(1,1,4.54),
	(1,2,3.34),
	(2,3,4.19),
	(2,11,1.12);

create table orders
(
	id int primary key identity (1,1) not null,
	state nvarchar (3) not null check (state in('st1', 'st2', 'st3')), -- аналог enum
	amount int not null
);

insert into 
	orders (state, amount)
values
	('st1', 10000),
	('st2', 3400),
	('st3', 7300);

create table products
(
	id int primary key identity (1,1) not null,
	name nvarchar (100) not null,
	count int not null,
	price decimal (7,2) not null
);

insert into
	products (name, count, price)
values
	('Холодильник', 10, 50000),
	('Стиральная машина', 0, 23570),
	('Утюг', 3, 7300);
	
create table users
(
	id int primary key identity (1,1) not null,
	first_name nvarchar (20) not null default '', --значение по умолчанию
	last_name nvarchar (20) null,
	birthday date not null
);

insert into
	users (last_name, birthday)
values
	( 'Иванов', '1995-08-12'),
	('Белый', '1993-07-08'),
	( 'Давыдов', '1996-12-23'),
	( null, '1996-12-23');

create table messages
(
	id int primary key identity (1,1) not null,
	subject nvarchar (100) not null,
	message text not null,
	add_date datetime not null,
	is_public bit not null
);

insert into 
	messages (subject, message, add_date, is_public)
values
 ('Первое сообщение', 'Это моё первое сообщение', '2016-12-12 15:12:36', 1)
