drop table if exists users, clients, products, posts, passports

create table users
(
	id int primary key identity (1,1) not null,
	user_id int null unique,
	first_name nvarchar (20) not null default '',
	last_name nvarchar (20) null,
	birthday date not null,
	serial_passport int not null,
	number_passport int not null,
	unique (serial_passport, number_passport), --указание нескольких гарантирует уникальность для всей строки
);

--Все уникальные ключи должны иметь те же имена, что и поля, для которых они создаются
create table clients
(
	id int primary key identity (1, 1),
	first_name nvarchar (50) not null,
	last_name nvarchar (50) not null,
	email nvarchar (100) not null unique,
	passport nvarchar (10) not null unique
);

--В рамках всего сайта slug могут повторяться, но для каждой конкретной категории значения slug уникальны. 
--Создайте подходящий под это условие ключ с именем category_slug.
create table products
(
	id int primary key identity (1, 1),
	category_id int null default null,
	name nvarchar (100) not null,
	slug nvarchar (50) not null,
	ean13 nvarchar (13) not null unique,
	unique (category_id, slug)
);

--В рамках всего сайта slug могут повторяться, но для каждого конкретного пользователя значения slug уникальны. 
--Создайте подходящий под это условие ключ с именем uslug.
create table posts 
(
	id int primary key identity (1, 1),
	user_id int not null,
	name nvarchar (100) not null,
	pub_date datetime null default null,
	slug nvarchar (50) not null,
	unique (user_id, slug)
);

create table passports
(
	id int primary key identity (1, 1),
	user_id int not null,
	series nvarchar (4) not null,
	number nvarchar (6) not null,
	state nvarchar (7) not null check (state in('active', 'expired')) default 'active'
	unique (series, number)
);

insert into 
	passports (user_id, series, number, state)
values 
	(34, 7517, 517590, 'active'),
	(34, 7518, 437590, 'active'),
	(34, 7519, 418590, 'expired'),
	(34, 7511, 417590, 'active'),
	(34, 7512, 417570, 'expired'),
	(34, 7513, 417591, 'active'),
	(34, 7514, 317590, 'active'),
	(34, 7515, 457590, 'active'),
	(34, 7516, 418990, 'expired'),
	(34, 7817, 417590, 'expired'),
	(34, 7917, 417580, 'expired'),
	(34, 7717, 417599, 'active'),
	(34, 7917, 917590, 'active'),
	(34, 7527, 457590, 'expired'),
	(34, 8537, 411590, 'active'),
	(34, 7947, 417390, 'expired'),
	(34, 7597, 517590, 'expired'),
	(34, 7514, 437590, 'active'),
	(34, 6519, 418590, 'active'),
	(34, 7511, 417990, 'active'),
	(34, 7542, 417570, 'expired'),
	(34, 7513, 917591, 'expired'),
	(34, 1514, 317590, 'expired'),
	(34, 7115, 457590, 'expired'),
	(34, 7523, 418990, 'active'),
	(34, 7337, 417590, 'expired'),
	(34, 7417, 417580, 'active'),
	(34, 7237, 417599, 'expired'),
	(34, 7912, 917590, 'active'),
	(34, 3527, 457590, 'expired'),
	(34, 7637, 411590, 'active'),
	(34, 7597, 417390, 'expired');




 

