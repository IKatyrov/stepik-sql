drop table if exists users, products, orders

create table users
(
	id int identity (1,1) primary key clustered  not null, -- primary key clustered (id asc), индекс можно создавать как для отдельного поля, так и для нескольких
	user_id int null unique,
	first_name nvarchar (20) not null default '',
	last_name nvarchar (20) null,
	birthday date not null,
	serial_passport int not null,
	number_passport int not null,
	unique (serial_passport, number_passport)
);

create table orders
(
	id int identity (1,1), 
	user_id int not null,
	state nvarchar (8) default 'new' not null,
	amount int CHECK (amount >= 0 and amount <= 1000000) default 0 not null,
	primary key clustered (id asc, user_id asc, state asc)
);

create table products
(
	id int identity (1,1),
	category_id int check (category_id >= 0) not null,
	name nvarchar (100) not null,
	count smallint check (count >= 0 and count <= 1000000) default 0 not null,
	price decimal (10,2) check (price >= 0) default 0.00 not null
	primary key clustered (id asc, category_id asc, price asc)
);





 

