CREATE TABLE products (
    id INT,
    name NVARCHAR(255) NULL,
    count INTEGER NULL,
    price INTEGER NULL
);
INSERT INTO products (id, name, count, price)
VALUES
    (1, 'Стиральная машина', 5, 10000),
    (2, 'Холодильник', 0, 10000),
    (3, 'Микроволновка', 3, 4000),
    (4, 'Пылесос', 2, 4500),
    (5, 'Вентилятор', 0, 700),
    (6, 'Телевизор', 7, 31740);

CREATE TABLE users (
    id INT,
    first_name NVARCHAR(50) NULL,
    last_name NVARCHAR(50) NULL,
    birthday DATE NULL
);
INSERT INTO users (id, first_name, last_name, birthday)
VALUES
    (1, 'Дмитрий', 'Петров', '2000-03-14'),
    (2, 'Ольга', 'Антонова', '1999-12-01'),
    (3, 'Сергей', 'Васильев', '2002-02-20'),
    (4, 'Константин', 'Степаниденко', NULL),
    (5, 'Алена', 'Шикова', '1999-08-17'),
    (6, 'Василина', 'Парамонова', '2000-10-10'),
    (7, 'Александр', 'Пузаков', '2002-02-20'),
    (8, 'Алина', 'Антонова', '2002-01-01');


CREATE TABLE orders (
    id INT NOT NULL PRIMARY KEY,
    products INTEGER NULL,
    sum INTEGER NULL
);
INSERT INTO orders (id, products, sum)
VALUES
    (1, 2, 1300),
    (2, 1, 10000),
    (3, 1, 2140),
    (4, 5, 6800),
    (5, 1, 999);
	
--данные можно вставить по разному.
-- способ 1: INSERT INTO table (field1, field2) VALUES (value1, value2); 
-- способ 2: INSERT INTO table (field1, field2) 
				--VALUES 
				--(value1_1, value1_2),
				--(value2_1, value2_2),
				--(value3_1, value3_2);

--Добавьте в таблицу orders данные о новом заказе стоимостью 3000 рублей. В заказе 3 товара (products).
insert into orders (orders.id, orders.products, orders.sum)
values (6, 7, 30000)

--Добавьте в таблицу products новый товар — «Xbox», стоимостью 30000 рублей в количестве (count) трех штук
insert into products (products.id, products.name, products.count, products.price)
values (7, 'iMac 21', 0, 100100)

--Добавьте в таблицу users нового пользователя Антона Пепеляева с датой рождения 12 июля 1992 года
insert into users (users.id, users.first_name, users.last_name, users.birthday)
values(9, 'Антон', 'Пепелев', '1992-07-12')

/*
* Добавьте одним SQL запросом в таблицу products следующие товары:
* iPhone 7, цена 59990, 1 шт.
* iPhone 8, цена 64990, 3 шт.
* iPhone X, цена 79900, 2 шт.
*/
insert into products (products.id, products.name, products.count, products.price)
values 
	(8, 'iPhone 7', 1, 59990),
	(9, 'iPhone 8', 3, 64990),
	(10, 'iPhone X', 2 ,79900)





