CREATE TABLE products (
    id INT NOT NULL PRIMARY KEY,
    name NVARCHAR(255) NULL,
    count INTEGER NULL,
    price INTEGER NULL
);
INSERT INTO products (id, name, count, price)
VALUES
    (1, 'Стиральная машина', 5, 10000),
    (2, 'Холодильник', 0, 10000),
    (3, 'Микроволновка', 3, 4000),
    (4, 'Пылесос', 2, 4500),
    (5, 'Вентилятор', 0, 700),
    (6, 'Телевизор', 7, 31740),
    (7, 'IMAC', 3, 109990),
    (8, 'iPhone 7', 1, 59990),
    (9, 'iPhone 8', 3, 64990),
	(10, 'iPhone X', 3, 79900),
	(11, 'Сникерс', 4, 50),
    (12, 'Марс', 2, 55);

CREATE TABLE orders (
    id INT NOT NULL PRIMARY KEY,
    user_id INTEGER NULL,
    products_count INTEGER NULL,
    sum INTEGER NULL,
    status NVARCHAR(20) NULL,
    amount INTEGER NULL
);
INSERT INTO orders (id, user_id, products_count, sum, status, amount)
VALUES
    (1, 1, 2, 1300, 'new', 2600),
    (2, 18, 1, 10000, 'cancelled', 0),
    (3, 11, 1, 2140, 'in_progress', NULL),
    (4, 145, 5, 6800, 'new', 34000),
    (5, 23, 1, 999, 'new', 999),
    (6, 1, 2, 7690, 'cancelled', NULL),
    (7, 17, 1, 1600, 'new', 0),
    (8, 5, 4, 400, 'delivery', NULL),
    (9, 2355, 1, 1450, 'null', 1450),
    (10, 13, 7, 13000, 'cancelled', 91000);

CREATE TABLE users (
    id INT NOT NULL PRIMARY KEY,
    first_name NVARCHAR(50) NULL,
    last_name NVARCHAR(50) NULL,
    birthday DATE NULL,
    salary INTEGER NULL
);
INSERT INTO users (id, first_name, last_name, birthday, salary)
VALUES
    (1, 'Дмитрий', 'Петров', '2000-03-14', 17000),
    (2, 'Ольга', 'Антонова', '1999-12-01', 20000),
    (3, 'Сергей', 'Васильев', '2002-02-20', 15000),
    (4, 'Константин', 'Степаниденко', NULL, 35000),
    (5, 'Алена', 'Шикова', '1999-08-17', 40000),
    (6, 'Василина', 'Парамонова', '2000-10-10', 18500),
    (7, 'Александр', 'Пузаков', '2002-02-20', 21000),
    (8, 'Алина', 'Антонова', '2002-01-01', 55000),
    (9, 'Антон', 'Пепеляев', '1992-07-12', 60000);
	

--В таблицу products внесли данные с ошибкой, вместо iMac в наименовании написали IMAC. Исправьте ошибку.
update products set name='iMac' where name='IMAC';

--Увеличьте цену 5 самых дешевых товаров на 5%.
with temp_table as
(
	select top (5)  *			--получаю первые 5
	from  products				--в таблице products
	order by products.price		--отсортированые по цене по возрастанию
)
update temp_table
 set price = price * 1.05;

 --В поле amount в таблице orders должно стоять число, которое равно произведению цены (sum) на количество (products_count). 
 --Но из-за сбоя в системе некоторые значения суммы получили 0 или NULL. 
 --Обновите таблицу, чтобы в поле amount были правильные значения.
 /*
	orders (id, user_id, products_count, sum, status, amount)
    (1, 1, 2, 1300, 'new', 2600),
    (2, 18, 1, 10000, 'cancelled', 0),
    (3, 11, 1, 2140, 'in_progress', NULL),
	(4, 145, 5, 6800, 'new', 34000),
 */

 with temp_table as
(	
	select *							--получаю все записи 
	from  orders						--в таблице orders
	where amount = null or amount = 0	--где amount == null и amount == 0
)
update temp_table
 set amount = sum * products_count;

 --Измените статус (status) заказа под номером (id) 5 с delivery на success.
 update orders set orders.status = 'success' where orders.id = 5;

 --Увеличьте в таблице users сотрудникам, у которых зарплата менее 20 000 рублей, зарплату (salary) на 10%.
 update users set users.salary = users.salary * 1.1 where users.salary <= 20000;

 --Проставьте всем заказам без статуса (status равен NULL) статус "new".
 update orders set orders.status = 'new' where orders.status = 'null';

 --Уменьшите цену 5 самых дорогих товаров на 5000 рублей.
 with temp_table as
 (
	select top (5) *
	from products
	order by products.price desc
 )
 update temp_table
 set price = price - 5000;

--Ниже находится таблица с товарами в магазине. В поле count содержится текущее количество товаров на полках и на складе.
--В магазин привезли 2 упаковки Сникерса и 2 упаковки Марса. В каждой упаковке по 20 шоколадок. Обновите данные так, чтобы они отражали количество шоколадок с учетом нового привоза.
with temp_table as
(
	select *
	from products
	where products.name in ('Марс', 'Сникерс')
)
update temp_table
set count = count + (2 * 20)
