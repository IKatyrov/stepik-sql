CREATE TABLE cars (
    id INT NOT NULL PRIMARY KEY IDENTITY (1, 1),
    name NVARCHAR(255) NULL,
    year INT NULL,
    country NVARCHAR(2) NULL,
    power INTEGER NULL
);
INSERT INTO cars (name, year, country, power)
VALUES
    ('Toyota Camry', 2012, 'JP', 139),
    ('Mazda Demio', 2004, 'JP', 113),
    ('ВАЗ 2110', 2010, 'RU', 79),
    ('Nissan Almera', 2016, 'JP', 130),
    ('Nissan Juke', 2016, 'JP', 120),
    ('Reno Logan', 2009, 'FR', 87),
    ('Lada Priora', 2017, 'RU', 78),
    ('Reno Duster', 2011, 'FR', 143),
    ('Hyundai Solaris', 2010, 'KR', 98),
    ('Nissan Patrol', 2014, 'JP', 200),
    ('Reno Logan', 2011, 'FR', 90),
    ('Nissan Sunny', 1990, 'JP', 67);
	
CREATE TABLE products (
    id INT NOT NULL PRIMARY KEY IDENTITY (1, 1),
    name NVARCHAR(255) NULL,
    count INTEGER NULL
);
INSERT INTO products (name, count)
VALUES
    ('Баунти', 50),
    ('Твикс', 33),
    ('Сникерс', 0),
    ('Пикник', 25),
    ('Марс', 0),
    ('Милка', 18),
    ('Альпен Голд', 13),
    ('Дав', 0),
    ('Кит-кат', 38),
    ('Зебра', 11);

CREATE TABLE visits (
    id INT NOT NULL PRIMARY KEY IDENTITY (1, 1),
    user_id INTEGER NULL,
    date DATETIME NULL
);
INSERT INTO visits (user_id, date)
VALUES
    (1, '2017-01-02 12:23:03'),
    (18, '2017-01-02 14:23:24'),
    (11, '2017-01-03 12:17:50'),
    (145, '2017-01-06 18:37:01'),
    (1, '2017-01-07 12:12:08'),
    (4, '2017-01-07 12:12:14');
	
--Удалите из таблицы visits все данные с помощью конструкции DELETE
delete from visits;

--Удалите из таблицы products все товары, которых нет на складе
delete from products where products.count = 0

--Удалите из таблицы cars все автомобили начиная с 2010 года и старше
delete from cars where cars.year >= 2010

--Удалите из таблицы cars все корейские (country = "KR") автомобили, а также все автомобили мощностью (power) меньше 80 лс.
--Используйте один SQL-запрос.
delete from cars where cars.country = 'KR'or cars.power <= 80

--Удалите из таблицы cars все японские автомобили мощностью менее 80 и более 130 лс. (включая крайние значения).
with temp_table as
(
	select * 
	from cars
	where cars.country = 'JP'
)
delete from temp_table 
where temp_table.power <= 80 or temp_table.power >= 130;

--Удалите из таблицы cars все данные с помощью инструкции TRUNCATE (придется погуглить).
--TRUNCATE удаляет все данные из таблицы и ещё сбрасывает счётчик
truncate table cars
