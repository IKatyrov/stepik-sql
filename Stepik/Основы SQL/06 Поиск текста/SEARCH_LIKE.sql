drop table if exists  users

create table users 
(
    id int not null identity (1,1),
    first_name nvarchar(50) null,
    last_name nvarchar(50) null,
    age int NULL,
	constraint pk_id primary key clustered (id asc)
);
insert into users (first_name, last_name, age)
values
    ('Виктор', 'алтушев', 20),
    ('Светлана', 'Иванова', 17),
    ('Елена', 'Абрамова', 18),
    ('Василиса', 'Кац', 15),
    ('Антон', 'Сорокин', 22),
    ('Алёна', 'Алясева', 28),
    ('Лиана', 'Белая', 21),
    ('Карина', 'Белая', 30),
    ('Анастасия', 'Дейчман', 16),
    ('Юлия', 'Фёдорова', 25),
    ('Антон', 'Алтушев', 18);

select * from users where first_name like 'а%'; --найдёт все записи начинающиейся на а
select * from users where first_name like 'а%а'; --найдёт все записи начинающиейся и заканчиваяющиеся на а
select * from users where first_name like 'а____'; --5 андерскоров - найдёт все записи на а состоящие из 6 символов
select * from users where first_name like '______'; --6 андерскоров - найдёт все записи состоящие из 6 символов
select * from users where first_name not like '______'; --6 андерскоров - найдёт все записи состоящие не из 6 символов






