drop table if exists articles, films, products, states, category

create table states
(
	id int primary key identity (1,1)
);

create table articles
(
	id int not null identity (1,1),
	name nvarchar (80) not null,
	text nvarchar (255) not null,
	primary key clustered (id asc)
);

alter table articles
add state int not null,
constraint ck_state check (state in(1,2,3)), --constraint {name} check - явное добавление зависимости
constraint df_state default 1 for state, --constraint {name} default - явное добавление зависимости
constraint fk_state foreign key (state) references states(id) on delete cascade on update cascade;


--перед удалением поля нужно удалить все зависимости которые в нём присутствуют
alter table articles drop constraint ck_state; 
alter table articles drop constraint df_state;
alter table articles drop constraint fk_state;

alter table articles drop column state;

--------------------------------------------------------------

create table films 
(
    id int not null primary key identity (1,1),
    name nvarchar(50) not null,
    kinopoisk float not null constraint df_kinopoisk default 0,
    imdb float not null constraint df_imdb default 0,
    year date null
);

alter table films
add raiting decimal (5, 3) not null,
constraint df_raiting default 0.00 for raiting;

insert into films (name,kinopoisk,imdb)
values ('Зеленая миля', 9.135, 8.5);

update films
set raiting = ((kinopoisk + imdb) / 2);

--------------------------------------------------------
create table category
(
	id int not null identity (1,1),
	name nvarchar (50) not null,
	primary key clustered (id asc)
);

create table products
(
    id int not null identity (1,1),
    category_id int null constraint df_category_id default null, 
    name nvarchar(100) not null,
    count tinyint not null constraint df_count default 0, 
    price decimal (10,2) not null constraint df_price default 0.00,
	primary key clustered (id asc),
	constraint ck_price check (price in(0.00, 1,2,3)),
	constraint fk_category_id foreign key (category_id) references category(id)
);

alter table products
add stock_place nvarchar (6) not null,
constraint df_stock_place default '' for stock_place










